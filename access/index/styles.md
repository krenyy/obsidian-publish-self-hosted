Your slugs can be styled by plain CSS styles.

You can also just "borrow" a [`publish.css`][publish.css] from a site you like, for example, [The Quantum Well][quantum-well].

![[Pasted image 20211117094956.png]]

[publish.css]: https://publish-01.obsidian.md/access/14cfbb8788a60a83267c9a52339c8bdd/publish.css
[quantum-well]: https://publish.obsidian.md/myquantumwell/
