```json
{
  "defaultTheme": "light",
  "googleAnalytics": "",
  "indexFile": "",
  "logo": "",
  "noindex": false,
  "readableLineLength": true,
  "showBacklinks": false,
  "showGraph": true,
  "showHoverPreview": false,
  "showNavigation": true,
  "showOutline": false,
  "showSearch": false,
  "siteName": "",
  "slidingWindowMode": false,
  "strictLineBreaks": false
}
```

You can also just "borrow" an [options][options] file from a site you like, for example [The Quantum Well][quantum-well].

![[Pasted image 20211117101621.png]]
![[Pasted image 20211117101637.png]]

[options]: https://publish-01.obsidian.md/options/14cfbb8788a60a83267c9a52339c8bdd
[quantum-well]: https://publish.obsidian.md/myquantumwell/
